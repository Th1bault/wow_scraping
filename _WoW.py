#!/usr/local/bin/python3
import requests 
from lxml import html 
import time
import sys
import pickle
import os
import traceback
from progressbar import AnimatedMarker, Bar, BouncingBar, Counter, ETA, \
    AdaptiveETA, FileTransferSpeed, FormatLabel, Percentage, \
    ProgressBar, ReverseBar, RotatingMarker, \
    SimpleProgress, Timer, UnknownLength



def annoyingPrint(l):
    print("\033[1;31m\n:: Duplicate listing suppressions for \033[1;m",end='')
    sys.stdout.write("\033[1;32m")
    print(l,end='')
    print("\033[1;31m region ::\033[1;m",end='')
    print("")
    sys.stdout.flush()
    return 0
def preQueries (ask2,ccc,po,ro,reg,fo,data,toogo):
    global error
    global ierror
    error=0
    if ccc==1: queriesLauncher(ask2,ierror,po,ro,reg,fo,data)
    if ccc==0: queriesLauncher(ask2,"",po,ro,reg,fo,data)
    
    if error:
        if toogo==0:
            sys.stdout.write("\033[1;31m")
            ask4=input("\n\nError detected : pass or stop ? = ")
            while ask4 not in ['pass','stop']:
                ask4=input("Error detected : pass or stop ? = ")
            if ask4=="pass":
                sys.stdout.write("\033[1;34m")
                print("\nQueries miss the ",end='')
                sys.stdout.write("\033[1;35m")
                print(str(ierror),end='')
                sys.stdout.write("\033[1;34m")
                print(" element : ",end='')
                sys.stdout.write("\033[1;35m")
                ekpo=str(po[ierror]).encode('latin1').decode('utf8')
                print(str(ekpo),end='')
                ccc=1
                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)#####Mount problème + retour chariot erase
            if ask4=="stop":
                sys.stdout.write("\033[1;34m")
                print("\nQueries miss the ",end='')
                sys.stdout.write("\033[1;35m")
                print(str(ierror),end='')
                sys.stdout.write("\033[1;34m")
                print(" element : ",end='')
                sys.stdout.write("\033[1;35m")
                ekpo=str(po[ierror]).encode('latin1').decode('utf8')
                print(str(ekpo),end='')
        elif toogo:
            sys.stdout.write("\033[1;34m")
            print("\nQueries miss the ",end='')
            sys.stdout.write("\033[1;35m")
            print(str(ierror),end='')
            sys.stdout.write("\033[1;34m")
            print(" element : ",end='')
            sys.stdout.write("\033[1;35m")
            ekpo=str(po[ierror]).encode('latin1').decode('utf8')
            print(str(ekpo),end='')
            ccc=1
            preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
    return 0
def queriesLauncher(ask2,c,po,ro,reg,fo,data):
    global error
    global ierror
    ierror=0
    print("\n")
    sys.stdout.write("\033[1;33m")
    print("",end='')
    sys.stdout.flush()
    widgets = ['    ,ﾟ｡ ･ *. ﾟ☆  Completed: ', Counter(), ' players (', Timer(), ')  ☆ ﾟ.* ･ ｡ﾟ,']
    pbar = ProgressBar(widgets=widgets)
    with open('talents.pkl','rb') as f:
            l1,l2,l3 = pickle.load(f)
    if c :
        for i in pbar((i for i in range(c+1,len(po)))):
            error=0
            addPlayer(data,ro[i].encode('latin1').decode('utf8'),po[i].encode('latin1').decode('utf8'),reg[i],fo[i],i,l1,l2,l3)
            if error:
                ierror=i
                return 0
        data.close() 
    else :
        azerty=0
        if os.path.isfile("tmp.txt") :
           tmp=open('tmp.txt','r')
           azerty=tmp.read()
           tmp.close()
           os.remove("tmp.txt")
        for i in pbar((i for i in range(int(azerty),len(po)))):
            addPlayer(data,ro[i].encode('latin1').decode('utf8'),po[i].encode('latin1').decode('utf8'),reg[i],fo[i],i,l1,l2,l3)
            if error:
                ierror=i
                return 0
        data.close()
    return 0
def replace (ch,l,i):
    deb = ch[0:(i-1)]
    fin = ch[i:]
    return deb+l+fin
def headerArt():
    sys.stdout.write("\033[1;32m")
    print("""

 ██╗    ██╗ ██████╗ ██╗    ██╗    ██████╗  █████╗ ████████╗ █████╗ ██████╗  █████╗ ███████╗███████╗
 ██║    ██║██╔═══██╗██║    ██║    ██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔════╝
 ██║ █╗ ██║██║   ██║██║ █╗ ██║    ██║  ██║███████║   ██║   ███████║██████╔╝███████║███████╗█████╗  
 ██║███╗██║██║   ██║██║███╗██║    ██║  ██║██╔══██║   ██║   ██╔══██║██╔══██╗██╔══██║╚════██║██╔══╝  
 ╚███╔███╔╝╚██████╔╝╚███╔███╔╝    ██████╔╝██║  ██║   ██║   ██║  ██║██████╔╝██║  ██║███████║███████╗
  ╚══╝╚══╝  ╚═════╝  ╚══╝╚══╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ """)
    sys.stdout.write("\033[1;34m")
    print("""                ███████╗ ██████╗██████╗  █████╗ ██████╗ ██████╗ ██╗███╗   ██╗ ██████╗             
                ██╔════╝██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔══██╗██║████╗  ██║██╔════╝             
                ███████╗██║     ██████╔╝███████║██████╔╝██████╔╝██║██╔██╗ ██║██║  ███╗            
                ╚════██║██║     ██╔══██╗██╔══██║██╔═══╝ ██╔═══╝ ██║██║╚██╗██║██║   ██║            
                ███████║╚██████╗██║  ██║██║  ██║██║     ██║     ██║██║ ╚████║╚██████╔╝            
                ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝     ╚═╝╚═╝  ╚═══╝ ╚═════╝  
                """)
    sys.stdout.write("\033[1;36m")
    return 0
def addHeader(data):
    data.write(
    "\"Serveur\"\t\"Faction\"\t"
    "\"Joueur\"\t\"Royaume\"\t"
    "\"Score.pvp2\"\t\"Score.pvp3\"\t\"Score.pvp10\"\t"
    "\"Points.Haut.Fait\"\t"
    "\"Niveau d'objet\"\t"
    "\"Lvl\"\t\"Race\"\t\"Classe\"\t\"Spécialisation\"\t"
    "\"Talent.1\"\t\"Talent.2\"\t\"Talent.3\"\t\"Talent.4\"\t"
    "\"PV\"\t\"PM\"\t\"Intelligence\"\t\"Stamina\"\t\"CS.%\"\t\"Haste.%\"\t\"Mastery.%\"\t\"Versatility.%\"\t"
    "\"Honorable kills\"\t"
    "\"Ach.character.%\"\t\"Ach.Quests.%\"\t\"Ach.explo.%\"\t\"Ach.Pvp.%\"\t\"Ach.Dungeons_raids.%\"\t\"Ach.World_Events.%\"\t"
    "\"Pet.number\"\t\"Mount.number\"\n"
    )
    return 0
def addPlayer(data,realm,player,l,f,i,l1,l2,l3):
    global error
    while ' ' in realm:
        realm=replace(realm,"-",realm.find(" ")+1)
        if '(' in realm:
            realm=replace(realm,"",realm.find("(")+1)
            if ')' in realm:
                realm=replace(realm,"",realm.find(")")+1)
    while '\'' in realm:
        realm=replace(realm,"",realm.find("\'")+1)    
    try:
        pageContent=requests.get(
            'https://worldofwarcraft.com/'+str(l)+'/character/'+str(realm)+'/'+str(player)
        )
        tree = html.fromstring(pageContent.content)
        pageContent1=requests.get(
            'https://worldofwarcraft.com/'+str(l)+'/character/'+str(realm)+'/'+str(player)+'/achievements'
        )
        tree1 = html.fromstring(pageContent1.content)

        pageContent2=requests.get(
            'https://worldofwarcraft.com/'+str(l)+'/character/'+str(realm)+'/'+str(player)+'/collections/pets'
        )
        tree2 = html.fromstring(pageContent2.content)

        pageContent3=requests.get(
            'https://worldofwarcraft.com/'+str(l)+'/character/'+str(realm)+'/'+str(player)+'/collections/mounts'
        )
        tree3 = html.fromstring(pageContent3.content)
        if l=='ko-kr':
            realmb=["알렉스트라자","스톰레이지","윈드러너","데스윙","아즈샤라","달라란","노르간논","하이잘","듀로탄","가로나","굴단","세나리우스","줄진","말퓨리온","불타는-군단","헬스크림"]
            realma=["alexstrasza","stormrage","windrunner","deathwing","azshara","dalaran","norgannon","hyjal","durotan","garona","guldan","cenarius","zuljin","malfurion","burning-legion","hellscream"]
            realm=realma[realmb.index(realm)]
        woow=(str(l))
        woow+=("\t")
        woow+=(str(f))
        woow+=("\t")
        woow+=(str(player)+"\t"+str(realm)+"\t")
        k=0
        if tree.xpath("//div[@class='font-bliz-light-small-white']/text()"):
            if l=='fr-fr':#pvp.score
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[0].encode('latin1').decode('utf8')!="Aucune donnée de classement":
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[0].encode('latin1').decode('utf8')+"\t") #pvp2
                else:
                    woow+=("0\t")
                    k+=1     
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[1].encode('latin1').decode('utf8')!="Aucune donnée de classement":
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[1-k].encode('latin1').decode('utf8')+"\t") #pvp3
                else:
                    woow+=("0\t")
                    k+=1
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[2].encode('latin1').decode('utf8')=="Aucune donnée de classement":
                    woow+=("0\t")
                else:
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[2-k].encode('latin1').decode('utf8')+"\t") #pvp10
            if l=='en-us':#pvp.score
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[0].encode('latin1').decode('utf8')!="No Rated Play Data":
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[0].encode('latin1').decode('utf8')+"\t") #pvp2
                else:
                    woow+=("0\t")
                    k+=1     
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[1].encode('latin1').decode('utf8')!="No Rated Play Data":
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[1-k].encode('latin1').decode('utf8')+"\t") #pvp3
                else:
                    woow+=("0\t")
                    k+=1
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[2].encode('latin1').decode('utf8')=="No Rated Play Data":
                    woow+=("0\t")
                else:
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[2-k].encode('latin1').decode('utf8')+"\t") #pvp10
            if l=='ko-kr':#pvp.score
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[0].encode('latin1').decode('utf8')!="평점제 플레이 정보 없음":
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[0].encode('latin1').decode('utf8')+"\t") #pvp2
                else:
                    woow+=("0\t")
                    k+=1     
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[1].encode('latin1').decode('utf8')!="평점제 플레이 정보 없음":
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[1-k].encode('latin1').decode('utf8')+"\t") #pvp3
                else:
                    woow+=("0\t")
                    k+=1
                if tree.xpath("//div[@class='font-bliz-light-small-white']/text()")[2].encode('latin1').decode('utf8')=="평점제 플레이 정보 없음":
                    woow+=("0\t")
                else:
                    woow+=(tree.xpath("//div[@class='font-bliz-light-small-beige']/text()")[2-k].encode('latin1').decode('utf8')+"\t") #pvp10
        else : return 0    
        woow+=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[2]/div/div[1]/div[2]/div[1]/a[1]/div/div[2]/text()')[0].encode('latin1').decode('utf8')+"\t") #Haut Fait
        y=(str(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[2]/div/div[1]/div[2]/div[1]/a[2]/div/div[2]/text()')[0].encode('latin1').decode('utf8')+"\t"))#Level
        if 'Niveau d’objet ' in y:
            y=y.replace('Niveau d’objet','')
        if ' ilvl' in y:
            y=y.replace(' ilvl','')
        if '레벨' in y:
            y=y.replace('레벨','')
        woow+=(y)     
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[2]/div/div[1]/div[2]/div[2]/div[1]/text()')[0].encode('latin1').decode('utf8')) #Lvl, Race, Spé, Classe
        races=[ "Nain sombrefer","Draeneï sancteforge","Elfe du Vide","Tauren de Haut-Roc","Orc mag’har","Sacrenuit",
                "Humain","Nain","Elfe de la nuit","Gnome","Draeneï","Worgen","Pandaren",
                "Orc","Mort-vivant","Tauren","Troll","Elfe de sang","Gobelin"]
        racesen=[   "Dark Iron Dwarf","Lightforged Draenei","Void Elf","Highmountain Tauren","mag’har Orc","Nightborne",
                    "Human","Dwarf","Night Elf","Gnome","Draenei","Worgen","Pandaren",
                    "Orc","Undead","Tauren","Troll","Blood Elf","Goblin"]
        raceskr=[   "검은무쇠 드워프","빛벼림 드레나이","공허 엘프","높은산 타우렌","마그하르 오크","나이트본",
                    "인간","드워프","나이트 엘프","노움","드레나이","늑대인간","판다렌",
                    "오크","언데드","타우렌","트롤","블러드 엘프","고블린"]
        classes=["Guerrier","Paladin","Chasseur de démons","Chasseur","Voleur","Prêtre","Chevalier de la mort","Chaman","Mage","Démoniste","Moine","Druide"]
        classesen=["Warrior","Paladin","Demon Hunter","Hunter","Rogue","Priest","Death Knight","Shaman","Mage","Warlock","Monk","Druid"]
        classeskr=["전사","성기사","악마사냥꾼","사냥꾼","도적","사제","죽음의 기사","주술사","마법사","흑마법사","수도사","드루이드"]
        spe=[["Armes","Fureur","Protection"],
        ["Sacré","Protection","Vindicte"],
        ["Dévastation","Vengeance"],
        ["Maîtrise des bêtes","Précision","Survie"],
        ["Assassinat","Hors-la-loi","Finesse"],
        ["Discipline","Sacré","Ombre"],
        ["Sang","Givre","Impie"],
        ["Élémentaire","Amélioration","Restauration"],
        ["Arcanes","Feu","Givre"],
        ["Affliction","Démonologie","Destruction"],
        ["Maître brasseur","Tisse-brume","Marche-vent"],
        ["Équilibre","Farouche","Gardien","Restauration"]]

        speen=[["Arms","Fury","Protection"],
        ["Holy","Protection","Retribution"],
        ["Havoc","Vengeance"],
        ["Beast Mastery","Marksmanship","Survival"],
        ["Assassination","Outlaw","Subtlety"],
        ["Discipline","Holy","Shadow"],
        ["Blood","Frost","Unholy"],
        ["Elemental","Enhancement","Restoration"],
        ["Arcane","Fire","Frost"],
        ["Affliction","Demonology","Destruction"],
        ["Brewmaster","Mistweaver","Windwalker"],
        ["Balance","Feral","Guardian","Restoration"]]

        spekr=[["무기","분노","방어"],
        ["신성","보호","징벌"],
        ["파멸","복수"],
        ["야수","사격","생존"],
        ["암살","무법","잠행"],
        ["수양","신성","암흑"],
        ["혈기","냉기","부정"],
        ["정기","고양","복원"],
        ["비전","화염","냉기"],
        ["고통","악마","파괴"],
        ["양조","운무","풍운"],
        ["조화","야성","수호","회복"]]


        woow+=str(y[:y.index(" ")])+"\t"#Level
        y=y[y.index(" ")+1:]#Suppression
        for iiii in range(0,len(races)):
            if l=='fr-fr':
                if y.find(races[iiii]) >=0:
                    woow+=str(races[iiii])+"\t"#Race
                    y=y[y.index(races[iiii])+len(list(races[iiii]))+1:]#Suppression
                    break
            if l=='en-us':
                if y.find(racesen[iiii]) >=0:
                    woow+=str(races[iiii])+"\t"#Race
                    y=y[y.index(racesen[iiii])+len(list(racesen[iiii]))+1:]#Suppression
                    break
            if l=='ko-kr':
                if y.find(raceskr[iiii]) >=0:
                    woow+=str(races[iiii])+"\t"#Race
                    y=y[y.index(raceskr[iiii])+len(list(raceskr[iiii]))+1:]#Suppression
                    break
        for rrrr in range(0,len(classes)):
            if l=='fr-fr':
                if y.find(classes[rrrr]) >=0:
                    woow+=str(classes[rrrr])+"\t"#Race
                    y=y[:y.index(classes[rrrr])-1]#Suppression
                    oo=rrrr
                    woow+=spe[oo][spe[oo].index(y)]+"\t"
                    break
            if l=='en-us':
                if y.find(classesen[rrrr]) >=0:
                    if classesen[rrrr]==y[y.index(classesen[rrrr]):] and y[y.index(classesen[rrrr])-1]==" ":
                        woow+=str(classes[rrrr])+"\t"#Race
                        y=y[:y.index(classesen[rrrr])-1]#Suppression
                        oo=rrrr
                        woow+=spe[oo][speen[oo].index(y)]+"\t"
                        break
            if l=='ko-kr':
                if y.find(classeskr[rrrr]) >=0:
                    if classeskr[rrrr]==y[y.index(classeskr[rrrr]):] and y[y.index(classeskr[rrrr])-1]==" ":
                        woow+=str(classes[rrrr])+"\t"#Race
                        y=y[:y.index(classeskr[rrrr])-1]#Suppression
                        oo=rrrr
                        woow+=spe[oo][spekr[oo].index(y)]+"\t"
                        break
        if tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[4]/a/div/div[2]/div/text()'):                   
            if l=='en-us':
                woow+=(l1[l2.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[1]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T1
                woow+=(l1[l2.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[2]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T2
                woow+=(l1[l2.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[3]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T3
                woow+=(l1[l2.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[4]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T4
            if l=='ko-kr':
                woow+=(l1[l3.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[1]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T1
                woow+=(l1[l3.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[2]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T2
                woow+=(l1[l3.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[3]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T3
                woow+=(l1[l3.index(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[4]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))]+"\t") #T4
            if l=='fr-fr':
                woow+=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[1]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8')+"\t") #T1
                woow+=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[2]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8')+"\t") #T2
                woow+=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[3]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8')+"\t") #T3
                woow+=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[9]/div[1]/div[3]/div[2]/div/div[2]/div/div/div[4]/a/div/div[2]/div/text()')[0].encode('latin1').decode('utf8')+"\t") #T4
        else : 
            return 0
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[1]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #HP
        if ',' in y:
            y=y.replace(',','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[2]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #MP
        if ',' in y:
            y=y.replace(',','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[3]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #Int
        if ',' in y:
            y=y.replace(',','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[4]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #Stamina
        if ',' in y:
            y=y.replace(',','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[5]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #CS
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[6]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #Haste
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[7]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #Mastery
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[5]/div/div[8]/a/div/div[2]/span/text()')[0].encode('latin1').decode('utf8')+"\t") #Versatility
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree.xpath("//div[@class='font-semp-xSmall-white text-upper']/text()")[14].encode('latin1').decode('utf8')+"\t") #Honor kills
        if ',' in y:
            y=y.replace(',','')
        woow+=(y)
        y=(tree1.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[2]/div[1]/div/div/div/div[2]/div[1]/div/div/text()')[0].encode('latin1').decode('utf8')+"\t") #Ach.character
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree1.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[2]/div[2]/div/div/div/div[2]/div[1]/div/div/text()')[0].encode('latin1').decode('utf8')+"\t") #Ach.Quests
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        time.sleep(5)
        y=(tree1.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[2]/div[3]/div/div/div/div[2]/div[1]/div/div/text()')[0].encode('latin1').decode('utf8')+"\t") #Ach.explo
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree1.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[2]/div[4]/div/div/div/div[2]/div[1]/div/div/text()')[0].encode('latin1').decode('utf8')+"\t") #Ach.Pvp
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree1.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[2]/div[5]/div/div/div/div[2]/div[1]/div/div/text()')[0].encode('latin1').decode('utf8')+"\t") #Ach.Dungeons_raids
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        y=(tree1.xpath('/html/body/div[1]/div/div[3]/div[2]/div[4]/div/div[2]/div[8]/div/div/div/div[2]/div[1]/div/div/text()')[0].encode('latin1').decode('utf8')+"\t") #Ach.World_Events
        if '%' in y:
            y=y.replace('%','')
        woow+=(y)
        try:
            y=(tree2.xpath('/html/body/div[1]/div/div[3]/div[2]/div[5]/div/div[9]/div[1]/div/div[2]/div/text()')[0].encode('latin1').decode('utf8')+"\t")
            if ' mascottes différentes' in y:
                y=y.replace(' mascottes différentes','')
            if ' Unique Pets' in y:
                y=y.replace(' Unique Pets','')
            if '애완동물 ' in y:
                y=y.replace('애완동물 ','')
            if '마리(중복 제외)' in y:
                y=y.replace('마리(중복 제외)','')
            woow+=(y)
        except:
            woow+="0\t"
        try:
            y=(tree3.xpath('/html/body/div[1]/div/div[3]/div[2]/div[5]/div/div[3]/div[1]/div/div[2]/div/text()')[0].encode('latin1').decode('utf8'))
            if ' montures obtenues' in y:
                y=y.replace(' montures obtenues','')
            if ' Mounts Collected' in y:
                y=y.replace(' Mounts Collected','')
            if '탈것 ' in y:
                y=y.replace('탈것 ','')
            if '개 수집' in y:
                y=y.replace('개 수집','')
            woow+=y+"\n"
        except:
            woow+="0\n"
        data.write(woow)
        return 0
    except KeyboardInterrupt:
        print("\nProgram shutdown\n")
        sys.stdout.write("\033[1;32m")
        print("\n--> Program stops at ",end='')
        sys.stdout.write("\033[1;34m")
        print(str(i),end='')
        sys.stdout.write("\033[1;32m")
        print(" for player ",end='')
        sys.stdout.write("\033[1;34m")
        print(str(player),end='')
        print("\n")
        sys.stdout.write("\033[1;33m")
        eeee=1
        while eeee:
            ssss=input("Save Progress ? (y or n) = ")
            if ssss=="y":
                eeee=0
                tmp=open('tmp.txt','w')
                tmp.write(str(i))
                tmp.close()
            if ssss=="n":
                eeee=0
                data.write
        print("\n")
        sys.exit()
    except Exception:
        traceback.print_exc()
        error=+1
        return 0
def makeList(pageContent,tree,p,r,f,fac,a,pbar):
    for i in range(1,102):
        if (tree.xpath('/html/body/div[1]/div/div[5]/div[2]/div[8]/div[1]/div/div/div[2]/div['+str(i)+']/div[3]/a[1]/div/div[3]/div[2]/text()')):
            p.append(tree.xpath('/html/body/div[1]/div/div[5]/div[2]/div[8]/div[1]/div/div/div[2]/div['+str(i)+']/div[3]/a[1]/div/div[3]/div[2]/text()')[0])
            r.append(tree.xpath('/html/body/div[1]/div/div[5]/div[2]/div[8]/div[1]/div/div/div[2]/div['+str(i)+']/div[3]/a[1]/div/div[3]/div[4]/text()')[0])
            a.append(0)
            if fac=='alliance':
                f.append('alliance')
            elif fac=='horde':
                f.append('horde')
            pbar.update(len(a)+1)
    return p,r,f,a,pbar
def makeTable(l,tpvp,faction,p1,r1,f):
    sys.stdout.write("\033[1;37m")
    print("\nAlliance Request for "+str(tpvp),end='')
    sys.stdout.write("\033[1;33m")
    print(" ")
    if tpvp=='10v10':
        tpvp='battlegrounds'
    a=[]
    pbar = ProgressBar(widgets=[Percentage(), Bar(),Timer()], maxval=1050).start()
    for page in range(1,11):
        pageContent=requests.get(
                'https://worldofwarcraft.com/'+str(l)+'/game/pvp/leaderboards/'+str(tpvp)+'?faction='+str(faction[0])+'&page='+str(page)
            )
        tree = html.fromstring(pageContent.content)
        makeList(pageContent,tree,p1,r1,f,faction[0],a,pbar)
    pbar.finish()
    sys.stdout.write("\033[1;37m")
    if tpvp=='battlegrounds': print("Horde Request for 10v10",end='')  
    else : print("Horde Request for "+str(tpvp),end='')
    sys.stdout.write("\033[1;33m")
    print(" ")
    a=[]
    pbar = ProgressBar(widgets=[Percentage(), Bar(),Timer()], maxval=1050).start()
    for page in range(1,11):
        pageContent=requests.get(
                'https://worldofwarcraft.com/'+str(l)+'/game/pvp/leaderboards/'+str(tpvp)+'?faction='+str(faction[1])+'&page='+str(page)
            )
        tree = html.fromstring(pageContent.content)
        makeList(pageContent,tree,p1,r1,f,faction[1],a,pbar)
    pbar.finish()
    return p1,r1,f
def superTri(p1,p2,p3,r1,r2,r3,f1,f2,f3):
    red=[]
    pbar = ProgressBar(widgets=['Working: ', AnimatedMarker()], maxval=2*len(p1)).start()
    for i in range (0,len(p1)):
            pbar.update(i)
            for j in range (0,len(p2)):
                    
                    if p1[i]==p2[j]:
                        red.append(j)
    red.sort(reverse=True)
    for w in range(0,len(red)):
        del p2[red[w]]
        del r2[red[w]]
        del f2[red[w]]
    p1+=p2
    r1+=r2
    f1+=f2
    
    red1=[]
    for i in range (0,len(p1)):
            pbar.update(i)
            for j in range (0,len(p3)):
                    if p1[i]==p3[j]:
                        red1.append(j)
    red1.sort(reverse=True)
    for w in range(0,len(red1)):
        del p3[red1[w]]
        del r3[red1[w]]
        del f3[red1[w]]
    p1+=p3
    r1+=r3
    f1+=f3
    pbar.finish()
    return p1,r1,f1
def concac(p1,r1,f1,l,po,ro,fo,reg):
    po+=p1
    ro+=r1
    fo+=f1
    z=0
    while z<len(p1):
        reg.append(l)
        z+=1
    return po,ro,fo,reg

os.system('clear')
headerArt()
tpvp=['2v2','3v3','10v10']
faction=['alliance','horde']
p1,p2,p3=[],[],[]
r1,r2,r3=[],[],[]
f1,f2,f3=[],[],[]
po,ro,fo,reg=[],[],[],[]
ttt=1
lg=['fr-fr','en-us','ko-kr']

yn=input("\nDo you want to relaunch a Player database Request ? (all or none) = ")

while yn not in ['all','none'] and yn not in lg:
    yn=input("\nDo you want to relaunch a Player database Request ? (all or none) = ")

if yn=="all":
    for o in range(0,len(lg)):
        sys.stdout.write("\033[1;31m")
        print("\n:: Request Battle.net database for ",end='')
        sys.stdout.write("\033[1;32m")
        print(str(lg[o]),end='')
        sys.stdout.write("\033[1;31m")
        print(" region ::")
        makeTable(lg[o],tpvp[0],faction,p1,r1,f1)
        makeTable(lg[o],tpvp[1],faction,p2,r2,f2)
        makeTable(lg[o],tpvp[2],faction,p3,r3,f3)
        sys.stdout.write("\033[1;36m")
        with open('objs'+str(lg[o])+'.pkl', 'wb') as f:
            pickle.dump([p1,p2,p3,r1,r2,r3,f1,f2,f3], f)
        p1,p2,p3,r1,r2,r3,f1,f2,f3=[],[],[],[],[],[],[],[],[]

if yn in lg:
    yni=str()
    while yni != 'stop':
        sys.stdout.write("\033[1;31m")
        print("\n:: Request Battle.net database for ",end='')
        sys.stdout.write("\033[1;32m")
        print(str(yn),end='')
        sys.stdout.write("\033[1;31m")
        print(" region ::")
        makeTable(yn,tpvp[0],faction,p1,r1,f1)
        makeTable(yn,tpvp[1],faction,p2,r2,f2)
        makeTable(yn,tpvp[2],faction,p3,r3,f3)
        sys.stdout.write("\033[1;36m")
        with open('objs'+str(yn)+'.pkl', 'wb') as f:
            pickle.dump([p1,p2,p3,r1,r2,r3,f1,f2,f3], f)
        p1,p2,p3,r1,r2,r3,f1,f2,f3=[],[],[],[],[],[],[],[],[]
        yni=input("\nDo you want to relaunch another Player database Request ? ('stop' to skip) = ")
        while yni not in ['stop'] and yni not in lg:
            yni=input("\nDo you want to relaunch another Player database Request ? ('stop' to skip) = ")

if yn=="none":
    ttt=0
    for o in range(0,len(lg)):
        with open('objs'+str(lg[o])+'.pkl','rb') as f:
            p1,p2,p3,r1,r2,r3,f1,f2,f3 = pickle.load(f)
        annoyingPrint(lg[o])
        superTri(p1,p2,p3,r1,r2,r3,f1,f2,f3)
        concac(p1,r1,f1,lg[o],po,ro,fo,reg)
        p1,r1,f1=[],[],[]

if ttt:
    for o in range(0,len(lg)):
        with open('objs'+str(lg[o])+'.pkl','rb') as f:
            p1,p2,p3,r1,r2,r3,f1,f2,f3 = pickle.load(f)
        annoyingPrint(lg[o])
        superTri(p1,p2,p3,r1,r2,r3,f1,f2,f3)
        concac(p1,r1,f1,lg[o],po,ro,fo,reg)

sys.stdout.write("\033[1;34m")
print("\n\n--> The internal database contains ",end='')
sys.stdout.write("\033[1;35m")
print(str(len(po)),end='')
sys.stdout.write("\033[1;34m")
print(" players, for ",end='')
sys.stdout.write("\033[1;35m")
print(str(len(lg)),end='')
sys.stdout.write("\033[1;34m")
print(" regions\n")
sys.stdout.write("\033[1;36m")
u=1
while u:
    ask=input("Do you want to launch mass queries ? (y or n) = ")
    if ask=="y":
        u=0
        bbb=1
        while bbb:
            ask2=input("On which file do you want to write ? (data or test) = ")
            if ask2=="data" or ask2=="test":
                ccc=0
                global error
                global ierror
                ierror=0
                bbb=0
                error=0
                i=0

            if ask2=="data":
                aaa=1
                while aaa:
                    ask3=input("Overwrite data ? (y or n) = ")
                    if ask3=="y":
                        data=open('data_wow.txt','w')
                        addHeader(data)
                        aaa=0
                        zzzz=1
                        while zzzz:
                            ask10=input("Overpass error ? (y or n) = ")
                            if ask10=="y":
                                zzzz=0
                                toogo=1
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
                            if ask10=="n":
                                zzzz=0
                                toogo=0
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
                        
                    elif ask3=="n": 
                        data=open('data_wow.txt','a')
                        aaa=0
                        zzzz=1
                        while zzzz:
                            ask10=input("Overpass error ? (y or n) = ")
                            if ask10=="y":
                                zzzz=0
                                toogo=1
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
                            if ask10=="n":
                                zzzz=0
                                toogo=0
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)


            if ask2=="test":
                aaa=1
                while aaa:
                    ask3=input("Overwrite data ? (y or n) = ")
                    if ask3=="y":
                        data=open('test_wow.txt','w')
                        addHeader(data)
                        aaa=0
                        zzzz=1
                        while zzzz:
                            ask10=input("Overpass error ? (y or n) = ")
                            if ask10=="y":
                                zzzz=0
                                toogo=1
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
                            if ask10=="n":
                                zzzz=0
                                toogo=0
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
                        
                    elif ask3=="n": 
                        data=open('test_wow.txt','a')
                        aaa=0
                        zzzz=1
                        while zzzz:
                            ask10=input("Overpass error ? (y or n) = ")
                            if ask10=="y":
                                zzzz=0
                                toogo=1
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)
                            if ask10=="n":
                                zzzz=0
                                toogo=0
                                preQueries(ask2,ccc,po,ro,reg,fo,data,toogo)

    if ask=="n":
        u=0
print("\n")


